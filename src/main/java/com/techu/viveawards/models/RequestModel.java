package com.techu.viveawards.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "requests")  //nombre de tabla
public class RequestModel {

    @Id  //como la PK
    private String userId;
    private String awardId;

    public RequestModel() {
    }

    public RequestModel(String userId, String awardId) {
        this.userId = userId;
        this.awardId = awardId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }
}
