package com.techu.viveawards.repositories;

import com.techu.viveawards.models.AwardModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface AwardRepository extends MongoRepository<AwardModel, String> {

    @Query("{'puntos' : ?0 }")
    public List<AwardModel> findAllPuntos(int puntos);
}