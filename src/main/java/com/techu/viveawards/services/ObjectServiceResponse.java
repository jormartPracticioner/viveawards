package com.techu.viveawards.services;

import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.models.ErrorModel;
import com.techu.viveawards.models.UserModel;
import org.springframework.http.HttpStatus;

public class ObjectServiceResponse {

    private String msg;
    private ErrorModel error;
    private AwardModel awardModel;
    private UserModel userModel;
    private HttpStatus responseHttpStatusCode;

    public ObjectServiceResponse() {
    }

    public ObjectServiceResponse(String msg, ErrorModel error, AwardModel awardModel, UserModel userModel, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.error = error;
        this.awardModel = awardModel;
        this.userModel = userModel;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel error) {
        this.error = error;
    }

    public AwardModel getAwardModel() {
        return awardModel;
    }

    public void setAwardModel(AwardModel awardModel) {
        this.awardModel = awardModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
