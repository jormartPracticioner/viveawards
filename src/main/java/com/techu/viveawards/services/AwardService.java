package com.techu.viveawards.services;

import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.repositories.AwardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AwardService {

    @Autowired
    AwardRepository awardRepository;

    public List<AwardModel> findAll(){

        return this.awardRepository.findAll();
    }

    public AwardModel add(AwardModel award){
        System.out.println("addAward");

        return this.awardRepository.save(award);
    }

    public Optional<AwardModel> getById(String id){
        System.out.println("getByIdAward");

        return this.awardRepository.findById(id);


    }

    public AwardModel getByPuntos(int puntos){
        System.out.println("getByPuntos " + puntos);
        List<AwardModel> premios =  this.awardRepository.findAllPuntos(puntos);
        if(!premios.isEmpty())
        {
            return this.awardRepository.findAllPuntos(puntos).get(0);
        }
        else {
            return null;
        }
    }

    public boolean delete(String id){
        System.out.println("deleteAward");
        boolean result = false;

        if (this.awardRepository.findById(id).isPresent()){
            this.awardRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
