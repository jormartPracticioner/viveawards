package com.techu.viveawards.services;

import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.models.ErrorModel;
import com.techu.viveawards.models.RequestModel;
import com.techu.viveawards.models.UserModel;
import com.techu.viveawards.repositories.AwardRepository;
import com.techu.viveawards.repositories.RequestRepository;
import com.techu.viveawards.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RequestService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AwardRepository awardRepository;

    @Autowired
    AwardService awardService;

    @Autowired
    RequestRepository requestRepository;

    public List<RequestModel> findAll() {
        System.out.println("<RequestService> findAll");
        return this.requestRepository.findAll();
    }

    public Optional<RequestModel> findById(String id){
        System.out.println("<RequestService> findById");
        return this.requestRepository.findById(id);
    }

    public ObjectServiceResponse add(RequestModel request){
        System.out.println("<RequestService> add");

        ObjectServiceResponse result = new ObjectServiceResponse();
        ErrorModel errorModel = new ErrorModel();

        if (this.userRepository.findById(request.getUserId()).isPresent() == true)
        {
            System.out.println("<RequestService> OK: UserId encontrado");

            UserModel userModel = this.userRepository.findById(request.getUserId()).get();

            //Obtener la EDAD
            Integer edad = userModel.getEdad();
            System.out.println("<RequestService> Edad: "+edad);
            if(edad==null || edad < 1)
            {
                System.out.println("<RequestService> ERROR: edad no válida");
                result.setMsg("ERROR: Edad no valida: "+edad);
                result.setResponseHttpStatusCode(HttpStatus.CONFLICT);
                errorModel.setError("ERROR: Edad no valida: "+edad);
                result.setError(errorModel);
                return result;
            }

            //Obtener la ANTIGUEDAD
            Date fechaAlta = userModel.getFechaAlta();
            System.out.println("<RequestService> FechaAlta: "+fechaAlta);
            if(fechaAlta==null)
            {
                System.out.println("<RequestService> ERROR: fechaAlta no válida");
                result.setMsg("ERROR: fechaAlta no valida: "+edad);
                result.setResponseHttpStatusCode(HttpStatus.NOT_ACCEPTABLE);
                errorModel.setError("ERROR: fechaAlta no valida: "+edad);
                result.setError(errorModel);
                return result;
            }
            java.util.Date fechaActual = new Date();
            System.out.println("Año Alta "+fechaAlta.getYear());
            System.out.println("Año Actual "+fechaActual.getYear());
            Integer antiguedad = fechaActual.getYear() - fechaAlta.getYear();
            System.out.println("Antiguedad "+antiguedad);

            //Calculamos puntuacion en base a edad y fechaAlta
            Integer puntos = 0;
            Integer puntosEdad = 0;

            if(edad < 30){
                puntosEdad = 1;
            }else if(edad >=30 && edad <40){
                puntosEdad = 2;
            }else if(edad >=40 && edad <50){
                puntosEdad = 3;
            }else if(edad >=50){
                puntosEdad = 4;
            }
            Integer puntosAntiguedad = antiguedad / 5;
            puntos = puntosEdad + puntosAntiguedad;
            System.out.println("Puntos totales: "+puntosEdad+" (edad) + "+puntosAntiguedad+" (ant) = "+puntos);

            //Obtenemos premio => PENDIENTE: consulta en BBDD
            AwardModel awardModel = this.awardService.getByPuntos(puntos);
            if (awardModel != null) {
                System.out.println("<RequestService> OK: Premio encontrado");
                request.setAwardId(awardModel.getId());

                if (this.requestRepository.findById(request.getUserId()).isPresent() == true) {
                    System.out.println("<RequestService> ERROR: Usuario ya inscrito");
                    result.setMsg("ERROR: Usuario ya inscrito");
                    result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                    errorModel.setError("ERROR: Usuario ya inscrito");
                    result.setError(errorModel);
                    return result;
                }
                else
                {
                    //Insertamos en BBDD
                    this.requestRepository.save(request);

                    //Pendiente de devolver el premio
                    result.setAwardModel(awardModel);
                    result.setMsg(awardModel.getAwards_name()+" ("+awardModel.getAwards_desc()+")");
                    result.setResponseHttpStatusCode(HttpStatus.OK);
                }

            } else {
                System.out.println("<RequestService> ERROR: Premio no encontrado para "+puntos+" puntos");
                result.setMsg("ERROR: Premio no encontrado para "+puntos+" puntos");
                result.setResponseHttpStatusCode(HttpStatus.FORBIDDEN);
                errorModel.setError("ERROR: Premio no encontrado para "+puntos+" puntos");
                result.setError(errorModel);
                return result;
            }
        }
        else {
            System.out.println("<RequestService> ERROR: UserId no encontrado");
            result.setMsg("ERROR: Usuario no encontrado: "+request.getUserId());
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            errorModel.setError("ERROR: Usuario no encontrado: "+request.getUserId());
            result.setError(errorModel);
            return result;
        }
        return result;
    }

    public RequestModel update(RequestModel request){
        System.out.println("<RequestService> update");
        return this.requestRepository.save(request);
    }

    public boolean delete(String id){

        System.out.println("<RequestService> delete");

        boolean result = false;

        if(this.requestRepository.findById(id).isPresent() == true) {
            System.out.println("<RequestService> Request encontrada, borrando");
            this.requestRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
