package com.techu.viveawards.services;

import com.techu.viveawards.models.AwardModel;
import com.techu.viveawards.models.ErrorModel;
import org.springframework.http.HttpStatus;

public class RequestServiceResponse {

    private String msg;
    private ErrorModel error;
    private AwardModel awardModel;
    private HttpStatus responseHttpStatusCode;

    public RequestServiceResponse() {
    }

    public RequestServiceResponse(String msg, ErrorModel error, AwardModel awardModel, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.error = error;
        this.awardModel = awardModel;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ErrorModel getError() {
        return error;
    }

    public void setError(ErrorModel error) {
        this.error = error;
    }

    public AwardModel getAwardModel() {
        return awardModel;
    }

    public void setAwardModel(AwardModel awardModel) {
        this.awardModel = awardModel;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
