package com.techu.viveawards.controllers;

import com.techu.viveawards.models.UserModel;
import com.techu.viveawards.services.ObjectServiceResponse;
import com.techu.viveawards.services.UserService;
import jdk.dynalink.Operation;
import jdk.dynalink.linker.LinkerServices;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/viveawards")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
public class UserController {

    @Autowired
    UserService userService;

    //GET con id
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La id del empleado a buscar es: " + id);

        ObjectServiceResponse result = this.userService.findById(id);

        return new ResponseEntity<>(
                (result.getError()!=null)?result.getError():result.getUserModel(), result.getResponseHttpStatusCode());
    }


    //GET de todos los empleados
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers() {
        System.out.println("getUsers");

        //return this.productService.findAll();
        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }

    //POST de un empleado
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUsers(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del empleado que se va a crear es " + user.getId());
        System.out.println("El nombre del empleado que se va a crear es " + user.getNombre());
        System.out.println("Los apellidos del empleado que se va a crear es " + user.getApellidos());
        System.out.println("La edad del cliente que se va a crear es " + user.getEdad());
        System.out.println("La fechaAlta del cliente que se va a crear es " + user.getFechaAlta());

        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }

    //PUT de un empleado
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del empleado que se va a actualizar es " + user.getId());
        System.out.println("El nombre del empleado que se va a actualizar es " + user.getNombre());
        System.out.println("Los apellidos del empleado que se va a actualizar es " + user.getApellidos());
        System.out.println("La edad del cliente que se va a actualizar es " + user.getEdad());
        System.out.println("La fechaAlta del cliente que se va a crear es " + user.getFechaAlta());

        ObjectServiceResponse result = this.userService.findById(id);

        if (result.getUserModel()!=null){
            System.out.println("Empleado para actualizar encontrado, acutalizando");
            this.userService.update(user);
        }

        return new ResponseEntity(user,
                result.getUserModel()!=null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    //DELETE de un empleado
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del empleado a borrar es " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Empleado borrado" : "Empleado no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
